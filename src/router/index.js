import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    scrollBehavior() {
        return window.scrollTo({ top: 0, behavior: "smooth" });
    },
    routes: [
        // Dashboards

        // {
        //     path: "/",
        //     name: "analytics",
        //     component: () => import("@/apps/dash-exemple/Analytics")
        // },
        {
            path: "/facturation",
            name: "facturation_section",
            component: () => import("@/apps/facturation/index")
        },
        {
            path: "/facturation/add-facturation",
            name: "facturation_section_add",
            component: () => import("@/apps/facturation/components/modalFactureadd.vue")
        },
        {
            path: "/",
            name: "board_section",
            component: () => import("@/apps/tableauDeBord/index"),
            
        },
        {
            path: "/my-profil",
            name: "profil_section",
            component: () => import("@/apps/profil/index")
        },
        {
            path: "/my-credits",
            name: "credits_section",
            component: () => import("@/apps/credits-de-co/index")
        },
        {
            path: "/my-events",
            name: "events_section",
            component: () => import("@/apps/evenements/index")
        },
        {
            path: "/reservations",
            name: "reservations_section",
            component: () => import("@/apps/reservation-salles/index")
        }
    ]
});
